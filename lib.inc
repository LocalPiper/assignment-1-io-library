global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy

%define newline_char `\n`
%define tab_char `\t`
%define space_char ` `

section .text
; Принимает код возврата и завершает текущий процесс
exit:
    mov     rax, 60
    syscall


; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .loop:
        cmp byte [rdi + rax], 0 ; EOL check
        je .done
        inc rax
        jmp .loop
    .done:
        ret ; к концу работы в rax хранится длина строки


; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rsi
    mov rdx, rax
    mov rdi, 1
    mov rax, 1
    syscall
    ret


; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, newline_char


; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rax, 1
    mov rdx, 1
    mov rdi, 1
    mov rsi, rsp
    syscall
    pop rdi
    ret


; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    test rdi, rdi ; проверка на знак
    jge print_uint
    neg rdi ; отрицательное число инвертируем
    push rdi ; сохраняем
    mov rdi, '-'
    call print_char ; выводим минус
    pop rdi


; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    push rdi ; сохраняем данные
    push rdx

    sub rsp, 32 ; аллоцируем (на 32, чтобы соблюдать ABI)

    mov r8, rsp
    add r8, 32 ; ставим указатель не на вершину стека, а вниз
    mov byte[r8], 0 ; нуль-терминатор в ячейку

    mov rax, rdi ; загоняем значение в рабочий регистр
    mov r9, 10 ; делитель (10-СС)

    .loop:
        xor rdx, rdx ; чистим rdx
        div r9 ; делим
        add dl, '0' ; переводим в ASCII-код
        dec r8 ; движемся по стеку вверх
        mov [r8], dl ; загоняем полученный символ в стек
        test rax, rax ; проверка на конец деления (если частное == 0)
        jnz .loop

    mov rdi, r8 ; указатель на начало числа в стеке передаем в rdi
    call print_string

    add rsp, 32 ; освобождаем память
    pop rdx ; возвращаем данные
    pop rdi
    ret


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
    .loop:
        ; слова в разных местах памяти, но могут быть равны по содержанию
        ; сравним посимвольно
        mov r8b, [rdi]
        mov r9b, [rsi]

        cmp r8b, r9b
        jne .strs_are_not_equal
        test r8b, r9b ; проверка на конец
        jz .strs_are_equal ; если конец и до этого цикл не обрывался, значит, строки равны
        test r8b, r8b
        jz .strs_are_not_equal
        test r9b, r9b
        jz .strs_are_not_equal

        inc rdi
        inc rsi ; берем следующие символы
        jmp .loop
    .strs_are_equal:
        mov rax, 1
        ret
    .strs_are_not_equal:
        xor rax, rax
        ret


; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    xor rdi, rdi
    mov rdx, 1
    push rax
    mov rsi, rsp
    syscall
    pop rax
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
        push r13
        push r12
        mov r12, rdi
        mov r13, rsi

        xor rcx, rcx

        .loop:
                push rcx
                call read_char
                pop rcx

                cmp al, 0x0
                je .eos
                cmp al, newline_char
                je .check
                cmp al, tab_char
                je .check
                cmp al, space_char
                je .check

                cmp rcx, r13
                jg .error

                mov byte[r12+rcx], al
                inc rcx
                jmp .loop

        .check:
                test rcx, rcx
                jz .loop
                jmp .eos

        .error:
                xor rax, rax
                jmp .done

        .eos:
                mov byte[r12+rcx], 0x0
                mov rax, r12
                mov rdx, rcx

        .done:
                pop r12
                pop r13
                ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rdx, rdx
    xor r8, r8
    .loop:
        mov r8b, byte[rdi+rdx]

        ; проверки на вхождение в диапазон чисел
        cmp r8b, '0'
        jl .done

        cmp r8b, '9'
        jg .done

        imul rax, 10 ; следующий символ
        sub r8b, '0' ; переводим из ASCII-кода в число
        add rax, r8
        inc rdx ; двигаем указатель
        jmp .loop

    .done:
        ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    mov r8b, byte[rdi]
    cmp r8b, '-'
    je .neg_num

    cmp r8b, '+'
    je .pos_num

    cmp r8b, '0'
    jl .error

    cmp r8b, '9'
    jg .error

    jmp .print

    .pos_num:
        inc rdi
        call parse_uint
        inc rdx
        ret
    .print:
        jmp parse_uint
    .neg_num:
        inc rdi
        call parse_uint
        inc rdx
        neg rax
        ret
    .error:
        xor rdx, rdx
        ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    push rdi
    push rsi
    push rdx
    call string_length
    pop rdx
    pop rsi
    pop rdi
    cmp rax, rdx
    jge .not_enough_mem
    xor rax, rax
.loop:
    cmp byte[rdi + rax], 0
    je .done

    mov r8b, byte[rdi + rax]
    mov byte[rsi + rax], r8b
    inc rax
    jmp .loop

.not_enough_mem:
    xor rax, rax
    ret
.done:
    mov byte[rsi + rax], 0
    ret
